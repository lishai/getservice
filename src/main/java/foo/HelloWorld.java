package foo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloWorld {
    @Path("/txt")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response bla() {
        return Response.ok("Hello World!").build();
    }

    @Path("/html")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response blaHtml(@QueryParam("text") String txt, @QueryParam("hsize") int size) {
        return Response.ok("<body><h1>Hello World!</h1>"+txt+"<br>"+size +"</body>").build();
    }
}
